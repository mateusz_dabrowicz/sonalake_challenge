Feature: Challenge task

  Scenario: Challenge scenario
    Given user is on calculator page
    When user close cookie law
     And user click on button with number three
     And user click on button with number five
     And user click on button multiple
     And user click on button with number nine
     And user click on button with number nine
     And user click on button with number nine
     And user click on button plus
     And user click on left bracket
     And user click on button with number one
     And user click on button with number zero
     And user click on button with number zero
     And user click on button division
     And user click on button with number four
     And user click on right bracket
     And user click on equals sign
    Then user should see result "34990"
    When user click on button clear
     And user click on button radians
     And user click on button cosine
     And user click on button Pi
     And user click on right bracket
     And user click on equals sign
    Then user should see result "-1"
    When user click on button clear
     And user click on button degrees
     And user click on button square
     And user click on button with number eight
     And user click on button with number one
     And user click on right bracket
     And user click on equals sign
    Then user should see result "9"
    When user click on dropdown history
    Then user should see results on history list
      |35*999+(100/4)|
      |cos(pi)       |
      |sqrt(81)      |