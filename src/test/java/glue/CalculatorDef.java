package glue;

import cucumber.api.Scenario;
import cucumber.api.java8.En;
import hooks.Hooks;
import io.cucumber.datatable.DataTable;
import pages.CalculatorPage;

public class CalculatorDef implements En {

    private CalculatorPage calculatorPage;

    public CalculatorDef() {

        Before(2, (Scenario scenario) ->
                calculatorPage = new CalculatorPage(Hooks.driver));

        Given("^user is on calculator page$", () ->
                calculatorPage.navigateToPage());

        When("^user click on button with number zero$", () ->
                calculatorPage.clickOnZero());

        When("^user click on button with number one$", () ->
                calculatorPage.clickOnOne());

        When("^user click on button with number three$", () ->
                calculatorPage.clickOnThree());

        When("^user click on button with number four$", () ->
                calculatorPage.clickOnFour());

        When("^user click on button with number five$", () ->
                calculatorPage.clickOnFive());

        When("^user click on button with number eight$", () ->
                calculatorPage.clickOnEight());

        When("^user click on button with number nine$", () ->
                calculatorPage.clickOnNine());

        When("^user click on button multiple$", () ->
                calculatorPage.clickOnMultiple());

        When("^user click on button plus$", () ->
                calculatorPage.clickOnPlus());

        When("^user click on left bracket$", () ->
                calculatorPage.clickOnBracketLeft());

        When("^user click on right bracket$", () ->
                calculatorPage.clickOnBracketRight());

        When("^user click on button division$", () ->
                calculatorPage.clickOnDivision());

        When("^user click on equals sign$", () ->
                calculatorPage.clickOnEquals());

        When("^user click on button clear$", () ->
                calculatorPage.clickOnClear());

        When("^user click on button radians$", () ->
                calculatorPage.clickOnRad());

        When("^user click on button degrees$", () ->
                calculatorPage.clickOnDeg());

        When("^user click on button cosine", () ->
                calculatorPage.clickOnCos());

        When("^user click on button Pi$", () ->
                calculatorPage.clickOnPi());

        When("^user click on button square$", () ->
                calculatorPage.clickOnSqrt());

        When("^user click on dropdown history$", () ->
                calculatorPage.clickOnDropdownHist());

        Then("^user should see result \"([^\"]*)\"$", (String result) ->
                calculatorPage.inputShouldHaveValue(result));

        Then("^user should see results on history list$", (DataTable list) ->
                calculatorPage.historyListShouldHaveValue(list.asList()));
    }
}
