package glue;

import cucumber.api.Scenario;
import cucumber.api.java8.En;
import hooks.Hooks;
import pages.GenericPage;

public class GenericDef implements En {

    GenericPage genericPage;

    public GenericDef() {

        Before(2, (Scenario scenario) ->
                genericPage = new GenericPage(Hooks.driver));

        When("user close cookie law", () ->
                genericPage
                        .waitForCookieLaw()
                        .closeCookieLaw());
    }
}
