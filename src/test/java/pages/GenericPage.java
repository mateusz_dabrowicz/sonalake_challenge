package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GenericPage extends AbstractPage {

    public GenericPage(WebDriver driver) {
        super(driver);
    }

    public GenericPage closeCookieLaw() {
        getDriver().findElement(By.id("cookiesaccept")).click();
        return this;
    }

    public GenericPage waitForCookieLaw() {
        driverWait().until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#cookiemodal div")));
        return this;
    }
}
