package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waiter.Waiter;

import java.util.List;

public class CalculatorPage extends AbstractPage {

    private static final String PAGE_URL = "http://web2.0calc.com/";

    private Waiter waiter = new Waiter();

    @FindBy(id = "Btn0")
    private WebElement buttonZero;
    @FindBy(id = "Btn1")
    private WebElement buttonOne;
    @FindBy(id = "Btn3")
    private WebElement buttonThree;
    @FindBy(id = "Btn4")
    private WebElement buttonFour;
    @FindBy(id = "Btn5")
    private WebElement buttonFive;
    @FindBy(id = "Btn8")
    private WebElement buttonEight;
    @FindBy(id = "Btn9")
    private WebElement buttonNine;
    @FindBy(id = "BtnMult")
    private WebElement buttonMultiple;
    @FindBy(id = "BtnPlus")
    private WebElement buttonPlus;
    @FindBy(id = "BtnParanL")
    private WebElement buttonParanLeft;
    @FindBy(id = "BtnParanR")
    private WebElement buttonParanRight;
    @FindBy(id = "BtnDiv")
    private WebElement buttonDivision;
    @FindBy(id = "BtnCalc")
    private WebElement buttonEquals;
    @FindBy(id = "input")
    private WebElement input;
    @FindBy(id = "BtnClear")
    private WebElement buttonClear;
    @FindBy(id = "trigorad")
    private WebElement buttonRad;
    @FindBy(id = "trigodeg")
    private WebElement buttonDeg;
    @FindBy(id = "BtnCos")
    private WebElement buttonCos;
    @FindBy(id = "BtnPi")
    private WebElement buttonPi;
    @FindBy(id = "BtnSqrt")
    private WebElement buttonSqrt;
    @FindBy(css = "div#hist span.glyphicon-chevron-down")
    private WebElement dropdownHist;
    @FindBy(id = "histframe")
    private WebElement frameHistory;

    public CalculatorPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void navigateToPage() {
        getDriver().navigate().to(PAGE_URL);
    }

    public void clickOnZero() {
        buttonZero.click();
    }

    public void clickOnOne() {
        buttonOne.click();
    }

    public void clickOnThree() {
        buttonThree.click();
    }

    public void clickOnFour() {
        buttonFour.click();
    }

    public void clickOnFive() {
        buttonFive.click();
    }

    public void clickOnEight() {
        buttonEight.click();
    }

    public void clickOnNine() {
        buttonNine.click();
    }

    public void clickOnMultiple() {
        buttonMultiple.click();
    }

    public void clickOnPlus() {
        buttonPlus.click();
    }

    public void clickOnBracketLeft() {
        buttonParanLeft.click();
    }

    public void clickOnBracketRight() {
        buttonParanRight.click();
    }

    public void clickOnDivision() {
        buttonDivision.click();
    }

    public void clickOnEquals() {
        buttonEquals.click();
    }

    public void clickOnClear() {
        buttonClear.click();
    }

    public void clickOnRad() {
        buttonRad.click();
    }

    public void clickOnDeg() {
        buttonDeg.click();
    }

    public void clickOnCos() {
        buttonCos.click();
    }

    public void clickOnPi() {
        buttonPi.click();
    }

    public void clickOnSqrt() {
        buttonSqrt.click();
    }

    public void clickOnDropdownHist() {
        dropdownHist.click();
    }

    public void inputShouldHaveValue(String expectedValue) {
        waiter.waitForElementAttributeEqualsString(input, "value", expectedValue, getDriver());
    }

    public void historyListShouldHaveValue(List<String> values) {
        for (String value : values) {
            waiter.waitForElementTextContainsString(frameHistory, value, getDriver());
        }
    }
}
